const Koa = require('koa');
const app = new Koa();
const server = require('http').createServer(app.callback());
const WebSocket = require('ws');
const wss = new WebSocket.Server({server});
const Router = require('koa-router');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');

app.use(bodyparser());
app.use(cors());
app.use(async function (ctx, next) {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} ${ctx.response.status} - ${ms}ms`);
});

const file_name = 'data.json'

function save(data) {
  var fs = require('fs');
  fs.writeFile(file_name, JSON.stringify(data), 'utf8', function(error) {
    if (error) {
      console.log("save file error: " + `${error}`)
    } else {
      console.log("saved data:\n" + `${JSON.stringify(data)}`)
    }
  });
}

function prepopulate(database) {
  console.log("PREPOPULATING...");
  for (let i = 0; i < 10; i++) {
    database.items.push({id: `${i}`, text: `Todo ${i}`, created: seconds(Date.now()), updated: seconds(Date.now())});
  }
  save(database)
}

function seconds(date) {
  return Math.floor(date/1000);
}

// persistency

var database = { items: [] };

// load data from database
var fs = require('fs');
fs.readFile(file_name, 'utf8', function readFileCallback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log("GOT DATA:\n" + `${data}`);

    try {
      database = JSON.parse(data);
    } catch (e) {
      let msg = "EXCEPTION CAUGHT while reading " + `${file_name}` + ":\n" + `${e}`
      console.error(msg);
      prepopulate(database);
    }
  }
});

// endpoints 
const router = new Router();

// create
router.post('/todos', ctx => {
  const text = ctx.request.body.text;

  if (text === undefined || text === null || text.length == 0) {
    ctx.response.body = {error: "text must not be empty"}
    ctx.response.status = 400;
    return;
  }

  const id = database.items.length;
  const new_item = {id: `${id}`, text: text, created: seconds(Date.now()), updated: seconds(Date.now())}

  database.items[id] = new_item
  ctx.response.body = new_item
  ctx.response.status = 200;
  save(database)
});

// read
router.get('/todos', ctx => {
  ctx.response.body = database.items.sort((e1, e2) => -(e1.updated - e2.updated));
  ctx.response.status = 200;
});

// update
router.put('/todos/:id', ctx => {
  const item = ctx.request.body;
  const id = ctx.params.id;

  const index = database.items.findIndex(el => el.id === id);
  if (id !== item.id || index === -1) {
    ctx.response.error = {text: 'Item not found'};
    ctx.response.status = 400;
  } else {
    item.updated = seconds(Date.now());
    database.items[index] = item;
    ctx.response.body = item;
    ctx.response.status = 200;
    save(database)
  }
});

// delete
router.delete('/todos/:id', ctx => {
  const id = ctx.params.id;

  const index = database.items.findIndex(el => el.id === id);
  if (index === -1) {
    ctx.response.body = {text: 'Item not found'};
    ctx.response.status = 400;
  } else {
    database.items.splice(index, 1);
    ctx.response.body = {status: 1};
    ctx.response.status = 200;
    save(database)
  }
});

app.use(router.routes());
app.use(router.allowedMethods());

server.listen(3000);
