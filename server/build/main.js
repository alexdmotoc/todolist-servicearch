require("source-map-support").install();
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

module.exports = require("fs");

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Users_alexdmotoc_Personal_Faculta_ubb_master_2016_2018_MASTER_an_2_sem2_service_oriented_arch_TodoList_server_node_modules_babel_runtime_regenerator__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Users_alexdmotoc_Personal_Faculta_ubb_master_2016_2018_MASTER_an_2_sem2_service_oriented_arch_TodoList_server_node_modules_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Users_alexdmotoc_Personal_Faculta_ubb_master_2016_2018_MASTER_an_2_sem2_service_oriented_arch_TodoList_server_node_modules_babel_runtime_regenerator__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var Koa = __webpack_require__(4);
var app = new Koa();
var server = __webpack_require__(3).createServer(app.callback());
var WebSocket = __webpack_require__(9);
var wss = new WebSocket.Server({ server: server });
var Router = __webpack_require__(7);
var cors = __webpack_require__(6);
var bodyparser = __webpack_require__(5);

app.use(bodyparser());
app.use(cors());
app.use(function () {
  var _ref = _asyncToGenerator(__WEBPACK_IMPORTED_MODULE_0__Users_alexdmotoc_Personal_Faculta_ubb_master_2016_2018_MASTER_an_2_sem2_service_oriented_arch_TodoList_server_node_modules_babel_runtime_regenerator___default.a.mark(function _callee(ctx, next) {
    var start, ms;
    return __WEBPACK_IMPORTED_MODULE_0__Users_alexdmotoc_Personal_Faculta_ubb_master_2016_2018_MASTER_an_2_sem2_service_oriented_arch_TodoList_server_node_modules_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            start = new Date();
            _context.next = 3;
            return next();

          case 3:
            ms = new Date() - start;

            console.log(ctx.method + ' ' + ctx.url + ' ' + ctx.response.status + ' - ' + ms + 'ms');

          case 5:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());

var file_name = 'data.json';

function save(data) {
  var fs = __webpack_require__(0);
  fs.writeFile(file_name, JSON.stringify(data), 'utf8', function (error) {
    if (error) {
      console.log("save file error: " + ('' + error));
    } else {
      console.log("saved data:\n" + ('' + JSON.stringify(data)));
    }
  });
}

function prepopulate(database) {
  console.log("PREPOPULATING...");
  for (var i = 0; i < 10; i++) {
    database.items.push({ id: '' + i, text: 'Todo ' + i, created: seconds(Date.now()), updated: seconds(Date.now()) });
  }
  save(database);
}

function seconds(date) {
  return Math.floor(date / 1000);
}

// persistency

var database = { items: [] };

// load data from database
var fs = __webpack_require__(0);
fs.readFile(file_name, 'utf8', function readFileCallback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log("GOT DATA:\n" + ('' + data));

    try {
      database = JSON.parse(data);
    } catch (e) {
      var msg = "EXCEPTION CAUGHT while reading " + ('' + file_name) + ":\n" + ('' + e);
      console.error(msg);
      prepopulate(database);
    }
  }
});

// endpoints 
var router = new Router();

// create
router.post('/todos', function (ctx) {
  var text = ctx.request.body.text;

  if (text === undefined || text === null || text.length == 0) {
    ctx.response.body = { error: "text must not be empty" };
    ctx.response.status = 400;
    return;
  }

  var id = database.items.length;
  var new_item = { id: '' + id, text: text, created: seconds(Date.now()), updated: seconds(Date.now()) };

  database.items[id] = new_item;
  ctx.response.body = new_item;
  ctx.response.status = 200;
  save(database);
});

// read
router.get('/todos', function (ctx) {
  ctx.response.body = database.items.sort(function (e1, e2) {
    return -(e1.updated - e2.updated);
  });
  ctx.response.status = 200;
});

// update
router.put('/todos/:id', function (ctx) {
  var item = ctx.request.body;
  var id = ctx.params.id;

  var index = database.items.findIndex(function (el) {
    return el.id === id;
  });
  if (id !== item.id || index === -1) {
    ctx.response.error = { text: 'Item not found' };
    ctx.response.status = 400;
  } else {
    item.updated = seconds(Date.now());
    database.items[index] = item;
    ctx.response.body = item;
    ctx.response.status = 200;
    save(database);
  }
});

// delete
router.delete('/todos/:id', function (ctx) {
  var id = ctx.params.id;

  var index = database.items.findIndex(function (el) {
    return el.id === id;
  });
  if (index === -1) {
    ctx.response.body = { text: 'Item not found' };
    ctx.response.status = 400;
  } else {
    database.items.splice(index, 1);
    ctx.response.body = { status: 1 };
    ctx.response.status = 200;
    save(database);
  }
});

app.use(router.routes());
app.use(router.allowedMethods());

server.listen(3000);

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(8);


/***/ },
/* 3 */
/***/ function(module, exports) {

module.exports = require("http");

/***/ },
/* 4 */
/***/ function(module, exports) {

module.exports = require("koa");

/***/ },
/* 5 */
/***/ function(module, exports) {

module.exports = require("koa-bodyparser");

/***/ },
/* 6 */
/***/ function(module, exports) {

module.exports = require("koa-cors");

/***/ },
/* 7 */
/***/ function(module, exports) {

module.exports = require("koa-router");

/***/ },
/* 8 */
/***/ function(module, exports) {

module.exports = require("regenerator-runtime");

/***/ },
/* 9 */
/***/ function(module, exports) {

module.exports = require("ws");

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }
/******/ ]);
//# sourceMappingURL=main.map