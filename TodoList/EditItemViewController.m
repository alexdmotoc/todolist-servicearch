//
//  EditItemViewController.m
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import "EditItemViewController.h"
#import "UIViewController+ErrorHandling.h"
#import "RemoteService.h"

@interface EditItemViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation EditItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.textView.text = self.item.text;
}

- (IBAction)didTapUpdate:(id)sender {
    self.item.text = self.textView.text;
    
    RemoteService *remoteService = [RemoteService sharedService];
    [remoteService updateItem:self.item completion:^(TodoItem *note, NSError *error) {
        if (error) {
            [self showError:error message:@"Update failed"];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (IBAction)didTapDelete:(id)sender {
    RemoteService *remoteService = [RemoteService sharedService];
    [remoteService deleteItem:self.item completion:^(NSError *error) {
        if (error) {
            [self showError:error message:@"Delete failed"];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

@end
