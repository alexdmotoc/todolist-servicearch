//
//  RemoteService.m
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import "TodoItem.h"
#import "RemoteService.h"

static NSString * const kConnectionString = @"http://localhost:3000";
static NSString * const kEndpointString = @"todos";

@implementation RemoteService

#pragma mark - Setup

+ (instancetype)sharedService {
    static RemoteService *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [self new];
    });
    
    return manager;
}

#pragma mark - REST API

- (void)createItemWithText:(NSString *)text completion:(void (^)(TodoItem *, NSError *))completion {
    NSData *data = [NSJSONSerialization dataWithJSONObject:@{@"text":text}
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:nil];
    
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:kEndpointString
                         requestMethod:POST
                                params:nil
                           requestBody:data
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                if (error) {
                                    completion(nil, error);
                                    return;
                                }
                                
                                if (statusCode == 200) {
                                    completion([TodoItem instantiateWithDictionary:result], nil);
                                } else {
                                    NSString *message = [NSString stringWithFormat:@"Invalid status code: %ld", statusCode];
                                    NSError *err = [[self class] generateErrorWithCode:statusCode
                                                                               message:message];
                                    
                                    completion(nil, err);
                                }
                            }];
}

- (void)fetchAllItemsWithCompletion:(void (^)(NSArray<TodoItem *> *, NSError *))completion {
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:kEndpointString
                         requestMethod:GET
                                params:nil
                           requestBody:nil
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                if (error != nil) {
                                    completion(nil, error);
                                    return;
                                }
                                NSArray *notes = [TodoItem arrayFromDictionaryArray:result];
                                completion(notes, nil);
                            }];
}

- (void)updateItem:(TodoItem *)item completion:(void (^)(TodoItem *, NSError *))completion {
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:[NSString stringWithFormat:@"%@/%@", kEndpointString, item.itemId]
                         requestMethod:PUT
                                params:nil
                           requestBody:[item toData]
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                if (error) {
                                    completion(nil, error);
                                    return;
                                }
                                
                                if (statusCode == 200) {
                                    completion([TodoItem instantiateWithDictionary:result], nil);
                                } else {
                                    NSString *message = [NSString stringWithFormat:@"Invalid status code: %ld", statusCode];
                                    NSError *err = [[self class] generateErrorWithCode:statusCode
                                                                               message:message];
                                    
                                    completion(nil, err);
                                }
                            }];
}

- (void)deleteItem:(TodoItem *)item completion:(void (^)(NSError *))completion {
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:[NSString stringWithFormat:@"%@/%@", kEndpointString, item.itemId]
                         requestMethod:DELETE
                                params:nil
                           requestBody:nil
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                if (error) {
                                    completion(error);
                                } else if (statusCode != 200) {
                                    NSString *message = [NSString stringWithFormat:@"Invalid status code: %ld", statusCode];
                                    NSError *err = [[self class] generateErrorWithCode:statusCode
                                                                               message:message];
                                    
                                    completion(err);
                                } else {
                                    completion(nil);
                                }
                            }];
}

// private logic

+ (NSError *)generateErrorWithCode:(NSInteger)code message:(NSString *)message {
    return [NSError errorWithDomain:@"com.alexmotoc.todolist"
                               code:code
                           userInfo:@{
                                      NSLocalizedDescriptionKey: message
                                      }];
}

@end
