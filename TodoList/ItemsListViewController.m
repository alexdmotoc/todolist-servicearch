//
//  ItemsListViewController.m
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import "TodoItem.h"
#import "ItemsListViewController.h"
#import "RemoteService.h"
#import "TableViewCell.h"
#import "EditItemViewController.h"
#import "UIViewController+ErrorHandling.h"

@interface ItemsListViewController () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<TodoItem *> *items;
@end

@implementation ItemsListViewController


#pragma mark - ViewController Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchItems];
}

#pragma mark - UITableView Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[TableViewCell reuseIdentifier]];
    
    [cell populateWithItem:self.items[indexPath.row]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

#pragma mark - Logic

- (void)fetchItems {
    self.items = [NSMutableArray array];
    
    [[RemoteService sharedService] fetchAllItemsWithCompletion:^(NSArray<TodoItem *> *notes, NSError *error) {
        if (error) {
            [self showError:error message:@"Failed to get list of items"];
            return;
        }
        [self.items addObjectsFromArray:notes];
        [self.tableView reloadData];
    }];
}

- (IBAction)didTapAddButton:(UIBarButtonItem *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Todo item"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Text";
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Done"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                NSString *text = alert.textFields.firstObject.text;
                                                [self addItemWithText:text];
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleDestructive
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)addItemWithText:(NSString *)text {
    [[RemoteService sharedService] createItemWithText:text
                                           completion:^(TodoItem *item, NSError *error) {
                                               if (error) {
                                                   [self showError:error message:@"Failed to create new item"];
                                                   return;
                                               }
                                               [self fetchItems];
                                           }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    TodoItem *note = self.items[indexPath.row];
    
    [(EditItemViewController *)segue.destinationViewController setItem:note];
}

#pragma mark - AlertControllerDelegate

@end
