//
//  UIViewController+ErrorHandling.h
//  NotesApp
//
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ErrorHandling)

- (void)showError:(NSError *)error message:(NSString *)message;

@end
