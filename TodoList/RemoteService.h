//
//  RemoteService.h
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import "BaseService.h"
#import <Foundation/Foundation.h>

@interface RemoteService : BaseService

+ (instancetype)sharedService;

- (void)createItemWithText:(NSString *)text completion:(void(^)(TodoItem *item, NSError *error))completion;
- (void)fetchAllItemsWithCompletion:(void (^)(NSArray <TodoItem *> *items, NSError *error))completion;
- (void)updateItem:(TodoItem *)item completion:(void (^)(TodoItem *item, NSError *error))completion;
- (void)deleteItem:(TodoItem *)item completion:(void (^)(NSError *))completion;

@end
