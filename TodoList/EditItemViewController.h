//
//  EditItemViewController.h
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodoItem.h"

@interface EditItemViewController : UIViewController

@property (strong, nonatomic) TodoItem *item;

@end
