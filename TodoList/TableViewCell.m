//
//  TableViewCell.m
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *updatedLabel;
@end

@implementation TableViewCell

+ (NSString *)reuseIdentifier {
    return @"tableViewCell";
}

- (void)populateWithItem:(TodoItem *)item {
    _contentLabel.text = item.text;
    
    NSDateFormatter *f = [NSDateFormatter new];
    f.dateStyle = NSDateFormatterMediumStyle;
    f.timeStyle = NSDateFormatterMediumStyle;
    
    _createdLabel.text = [NSString stringWithFormat:@"Created: %@", [f stringFromDate:item.created]];
    _updatedLabel.text = [NSString stringWithFormat:@"Updated: %@", [f stringFromDate:item.updated]];
}

@end
