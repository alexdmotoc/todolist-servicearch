//
//  TableViewCell.h
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodoItem.h"

@interface TableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;
- (void)populateWithItem:(TodoItem *)item;

@end
