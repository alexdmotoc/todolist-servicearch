//
//  TodoItem.h
//
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodoItem : NSObject <NSCoding>

@property (strong, nonatomic) NSString *itemId;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSDate *updated;
@property (strong, nonatomic) NSDate *created;

+ (instancetype)instantiateWithDictionary:(NSDictionary *)dict;
+ (NSArray *)arrayFromDictionaryArray:(NSArray *)dictionaryArray;
+ (NSArray *)dictionaryArrayFromItemsArray:(NSArray *)itemsArray;

- (NSDictionary *)toDictionary;
- (NSData *)toData;

@end
