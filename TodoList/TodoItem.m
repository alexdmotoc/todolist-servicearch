//
//  Note.m
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import "TodoItem.h"

@implementation TodoItem

+ (instancetype)instantiateWithDictionary:(NSDictionary *)dict {
    TodoItem *item = [self new];
    
    item.itemId = dict[@"id"];
    item.text = dict[@"text"];
    item.updated = [NSDate dateWithTimeIntervalSince1970:[dict[@"updated"] doubleValue]];
    item.created = [NSDate dateWithTimeIntervalSince1970:[dict[@"created"] doubleValue]];
    
    return item;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        self.itemId = [coder decodeObjectForKey:@"id"];
        self.text = [coder decodeObjectForKey:@"text"];
        self.updated = [coder decodeObjectForKey:@"updated"];
        self.created = [coder decodeObjectForKey:@"created"];
    }
    return self;
}

- (NSDictionary *)toDictionary {
    NSDictionary *dictionary = @{
                                 @"id":self.itemId,
                                 @"text":self.text,
                                 @"updated":@([self.updated timeIntervalSince1970]),
                                 @"created":@([self.created timeIntervalSince1970])
                                 };
    
    return dictionary;
}

+ (NSArray *)arrayFromDictionaryArray:(NSArray *)dictionaryArray {
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSDictionary *dictionary in dictionaryArray) {
        [array addObject:[TodoItem instantiateWithDictionary:dictionary]];
    }
    
    return array;
}

+ (NSArray *)dictionaryArrayFromItemsArray:(NSArray *)itemsArray {
    NSMutableArray *array = [NSMutableArray array];
    
    for (TodoItem *item in itemsArray) {
        [array addObject:[item toDictionary]];
    }
    
    return array;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.itemId forKey:@"id"];
    [encoder encodeObject:self.text forKey:@"text"];
    [encoder encodeObject:self.updated forKey:@"updated"];
    [encoder encodeObject:self.created forKey:@"created"];
}

- (NSData *)toData {
    return [NSJSONSerialization dataWithJSONObject:[self toDictionary]
                                           options:NSJSONWritingPrettyPrinted
                                             error:nil];
}

@end
