//
//  AppDelegate.h
//  Created by Alex Motoc
//  Copyright © 2018 Alexandru Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end
